# AppArmor Loader

## Overview

Helm chart based on the following repository: https://github.com/kubernetes/kubernetes/tree/master/test/images/apparmor-loader

At the moment, there is a [limitation](https://github.com/kubernetes/kubernetes/tree/master/test/images/apparmor-loader#limitations) on updating and deleting profiles.

## Profiles

Profiles can be added by overwriting `profiles` as the following:
```
profiles:
  profile-one: |-
    profile profile-one {
      file,
    }
  profile-two: |-
    profile profile-two {
      umount,
    }
```

## Further information

More information on the AppArmor profile language can be found in: 
 - [Quick guide](https://gitlab.com/apparmor/apparmor/-/wikis/QuickProfileLanguage)
 - [Full reference](https://gitlab.com/apparmor/apparmor/-/wikis/AppArmor_Core_Policy_Reference)
 - [Policy layout](https://gitlab.com/apparmor/apparmor/-/wikis/Policy_Layout)